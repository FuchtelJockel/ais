BOARD ?= arduino-nano
ARDUINO_NANO_BOOTLOADER = optiboot

APPLICATION = ais

# This has to be the absolute path to the RIOT base directory:
RIOTBASE ?= $(CURDIR)/..

USEMODULE += analog_util
USEMODULE += periph_adc
USEMODULE += periph_uart
USEMODULE += printf_float
USEMODULE += ztimer_sec

# Comment this out to disable code in RIOT that does safety checking
# which is not needed in a production environment but helps in the
# development process:
DEVELHELP ?= 1

# Change this to 0 show compiler invocation lines by default:
QUIET ?= 1

include $(RIOTBASE)/Makefile.include
