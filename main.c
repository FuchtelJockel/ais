/*
 * The Autonomous Irrigation System [AIS]
 */

#include <stdlib.h>

#include "analog_util.h"
#include "board.h"
#include "cpu_conf.h"
#include "periph/adc.h"
#include "periph/gpio.h"
#include "periph/uart.h"
#include "sched.h"
#include "stdio_uart.h"
#include "thread.h"
#include "thread_config.h"
#include "time_units.h"
#include "ztimer.h"
#include "msg.h"

#define PUMP_PIN ARDUINO_PIN_3
#define SERIAL_BAUDRATE 9600
#define COMMAND_LEN 10
#define MON_QUEUE_SIZE (1)

enum state
{
	IDLE,
	RUN,
	MON,
};

typedef struct
{
	char* name;
	float threshold;
	gpio_t valve;
	adc_t sensor;
	ztimer_now_t since;
	uint32_t amount;
} plant;

static int STATE = IDLE;
static char mon_thread_stack[THREAD_STACKSIZE_DEFAULT];
static kernel_pid_t mon_thread_pid;
static msg_t mon_queue[MON_QUEUE_SIZE];

static plant plants[] = {
	{ "falsche Holzrose", 1.4, ARDUINO_PIN_10, ADC_LINE(5), 0, 5 },
	{ "kleine Holzrose", 1.4, ARDUINO_PIN_12, ADC_LINE(3), 0, 5 },
	// { "kräftige Holzrose", 1.4, ARDUINO_PIN_10, ADC_LINE(1), 0, 5 },
};

float read_voltage(adc_t line)
{
	int32_t sample = adc_sample(line, ADC_RES_10BIT);
	return adc_util_mapf(sample, ADC_RES_10BIT, 0.0, 3.3);
}

void* monitor(void* arg)
{
	msg_t msg;
	(void)arg;
	msg_init_queue(mon_queue, MON_QUEUE_SIZE);
	while (1) {
		msg_try_receive(&msg);
		if (STATE != MON)
			thread_sleep();
		printf("line %d: ", msg.content.value);
		printf("%fV\n", read_voltage(ADC_LINE(msg.content.value)));
		ztimer_sleep(ZTIMER_SEC, 2);
	}
}

static void rx_cb(void* arg, uint8_t data)
{
	(void)arg;
	static char buf[COMMAND_LEN];
	static int i = 0;
	static msg_t msg;
	if (data == '\n') {
		if (strcasecmp(buf, "stop") == 0) {
			STATE = IDLE;
			puts(": idling");
		} else if (strcasecmp(buf, "start") == 0) {
			STATE = RUN;
			puts(": running");
		} else if (strcasecmp(buf, "list") == 0) {
			for (uint32_t i = 0; i < sizeof(plants) / sizeof(plant); i++) {
				printf(": [%d] ", i);
				printf("%s, ", plants[i].name);
				printf("%2fV, ", plants[i].threshold);
				printf("%ds\n", plants[i].amount);
			}
		} else if (strcasestr(buf, "mon")) {
			STATE = MON;
			char* num = strtok(buf, "mon");
			int id = atoi(num);
			printf(": monitoring %s\n", plants[id].name);
			thread_wakeup(mon_thread_pid);
			msg.content.value = plants[id].sensor;
			msg_try_send(&msg, mon_thread_pid);
		}
		i = 0;
		memset(buf, 0, sizeof(buf));
	} else if (data == '\b') {
		--i;
		buf[i] = 0;
	} else if (i < COMMAND_LEN) {
		buf[i] = data;
		++i;
	} else {
		i = 0;
		memset(buf, 0, sizeof(buf));
	}
}

void init(void)
{
	uart_init(0, SERIAL_BAUDRATE, rx_cb, NULL);
	puts("Initializing AIS");
	printf("state: %d\n", STATE);
	gpio_init(PUMP_PIN, GPIO_OUT);
	ztimer_acquire(ZTIMER_SEC);
	for (uint32_t i = 0; i < sizeof(plants) / sizeof(plant); i++) {
		adc_init(plants[i].sensor);
		gpio_init(plants[i].valve, GPIO_OUT);
		plants[i].since = ztimer_now(ZTIMER_SEC);
	}
	// enable external reference voltage
	ADMUX &= ~(1 << REFS0);
	ADMUX &= ~(1 << REFS1);
}

void water(plant* p)
{
	printf("watering %s\n", p->name);
	float voltage;
	do {
		uint32_t passed = (ztimer_now(ZTIMER_SEC) - p->since);
		if (5 < passed) {
			gpio_set(PUMP_PIN);
			ztimer_sleep(ZTIMER_SEC, 2);
			gpio_set(p->valve);
			ztimer_sleep(ZTIMER_SEC, p->amount);
			gpio_clear(p->valve);
			ztimer_sleep(ZTIMER_SEC, 1);
			gpio_clear(PUMP_PIN);
			p->since = ztimer_now(ZTIMER_SEC);
		}
		voltage = read_voltage(p->sensor);
	} while (voltage > p->threshold);
	puts("done");
}

int main(void)
{
	init();
	mon_thread_pid = thread_create(mon_thread_stack,
								   sizeof(mon_thread_stack),
								   THREAD_PRIORITY_MAIN + 1,
								   THREAD_CREATE_SLEEPING,
								   monitor,
								   NULL,
								   "monitor");

	while (1) {
		for (uint32_t i = 0; i < sizeof(plants) / sizeof(plant); i++) {
			if (STATE == RUN &&
				plants[i].threshold < read_voltage(plants[i].sensor)) {
				water(&plants[i]);
			}
			ztimer_sleep(ZTIMER_SEC, 1);
		}
	}
	ztimer_release(ZTIMER_SEC);
	return 0;
}
